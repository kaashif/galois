.PHONY: clean

all:
	pdflatex --shell-escape galois.tex

clean:
	rm -f *.aux *.log *.out *.nav *.snm
