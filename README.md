Presentation on Galois Theory
=============================
Read the actual presentation for details.

Compiling
---------
Run `make` to build galois.pdf, `make clean` to clean up all of the
.toc and .aux files.

License
-------
This work is under the CC-BY-SA 3.0 license, so you can use it, make
derivative works and so on, as long as your work is licensed under the
same license and bears my name. See the `LICENSE` file for the full
license.
